package br.com.lm.leroymerlin.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import br.com.lm.leroymerlin.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rlWrapper);
        Button btnStart = (Button) findViewById(R.id.btnStart);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) btnStart.getLayoutParams();
        int middle = (rl.getHeight())/2;
        double difMargin = btnStart.getHeight()*2.35;
        lp.bottomMargin=(int) (middle-difMargin);

    }


    public void searchActivity(View view){
        Intent intent = new Intent(this,SearchActivity.class);
        ActivityOptionsCompat opts = ActivityOptionsCompat.makeCustomAnimation(this,R.anim.slide_in_left,R.anim.slide_out_left);
        ActivityCompat.startActivity(this, intent, opts.toBundle());
    }

}
