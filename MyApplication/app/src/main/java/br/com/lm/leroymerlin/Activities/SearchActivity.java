package br.com.lm.leroymerlin.Activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import br.com.lm.leroymerlin.R;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                FrameLayout f = (FrameLayout) findViewById(R.id.flSearch);
                AlphaAnimation fade_in = new AlphaAnimation(0.0f, 1.0f);
                fade_in.setDuration(500);
                fade_in.setFillAfter(true);
                f.setVisibility(View.VISIBLE);
                f.startAnimation(fade_in);
            }
        }, 800);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void showResult(View view){
        int nConnections=0;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnected()) nConnections++;
        if (mobile.isConnected()) nConnections ++;

        if (nConnections==0){
            Toast.makeText(this, "Não há redes de dados disponíveis. Tente conectar o celular no Wi-fi.", Toast.LENGTH_SHORT).show();
            return;
        }

        EditText etSearch = (EditText) findViewById(R.id.etSearch);
        String search = etSearch.getText().toString();

        if(search==null || search.length()==0){
            Toast.makeText(this,"Por favor, informe o que você deseja localizar", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this,SearchResultActivity.class);
            Bundle params = new Bundle();
            params.putString("search", search);
            intent.putExtras(params);
            ActivityOptionsCompat opts = ActivityOptionsCompat.makeCustomAnimation(this,R.anim.slide_in_left,R.anim.slide_out_left);
            ActivityCompat.startActivity(this, intent, opts.toBundle());
        }

    }

}
