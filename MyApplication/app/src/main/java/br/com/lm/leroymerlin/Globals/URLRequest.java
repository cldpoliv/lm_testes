package br.com.lm.leroymerlin.Globals;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Claudio on 15/05/16.
 */
public class URLRequest {

    public String makeRequest(String urlAddress){
        HttpURLConnection con=null;
        URL url = null;
        StringBuilder builder = new StringBuilder();

        try {
            url = new URL(urlAddress);
            con=(HttpURLConnection) url.openConnection();
            InputStream is = con.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(isr);
            String line = null;
            while ((line=reader.readLine())!=null) {
                builder.append(line + "\n");
            }
            reader.close();


        } catch (Exception e) {

            Log.e("MakeRequest", e.toString());

        } finally {
            con.disconnect();

        }
        return builder.toString();

    }

    public Bitmap getImageBitmap(String url) {
        Bitmap bitmap = null;
        try {
            InputStream in = new URL(url).openStream();
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (Exception e) {
            Log.e("getImageBitmap",e.toString());
        }
        return bitmap;

    }
}
