package br.com.lm.leroymerlin.Activities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import br.com.lm.leroymerlin.Globals.Config;
import br.com.lm.leroymerlin.Globals.VolleySingleton;
import br.com.lm.leroymerlin.R;







public class SearchResultActivity extends AppCompatActivity {
    private String search;
    private Map<Integer, String> deptos;
    private TextView txtTitle;
    private TextView txtSubtitle;
    private HorizontalScrollView hsv;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setDeptos();
        txtTitle = (TextView) findViewById(R.id.txtInf);
        txtSubtitle = (TextView) findViewById(R.id.txtSeeMap);
        hsv = (HorizontalScrollView) findViewById(R.id.hsvImage);
        progressBar = (ProgressBar)  findViewById(R.id.progressBar);

        txtTitle.setVisibility(View.INVISIBLE);
        txtSubtitle.setVisibility(View.INVISIBLE);
        hsv.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);

        Bundle args = getIntent().getExtras();
        search=args.getString("search");
        //getSearch();
        new GetProducts().execute();

    }


    public String setSearch(String s){
        int tam = s.length();
        String temp="";
        for(int i=0; i < tam; i++) {
            if(s.substring(i,i+1).equals(" ")){
                temp=temp + "%20";
            } else {
                temp=temp + s.substring(i, i+1);
            }
        }
        return temp;
    }


    public void setDeptos(){
        deptos = new HashMap<Integer, String>();
        deptos.put(1, "Materiais");
        deptos.put(2, "Carpintaria e Madeira");
        deptos.put(3, "Eletricidade");
        deptos.put(4, "Ferramentas");
        deptos.put(5, "Carpetes e Tapetes");
        deptos.put(6, "Ceramica");
        deptos.put(7, "Sanitário");
        deptos.put(8, "Encanamento");
        deptos.put(9, "Jardim");
        deptos.put(10, "Ferragens");
        deptos.put(11, "Pintura");
        deptos.put(12, "Decoração");
        deptos.put(13, "Iluminação");
        deptos.put(14, "Organização");
        deptos.put(15, "Cozinhas");
    }


    public void newSearch(View view){
        finish();
    }

    public Context getContext(){
        return this;
    }



    //Código utilizando volley. Não funciona pois não permite circular redirect
    public void getSearch() {
        String url = String.format(Config.term, search);
        RequestQueue queue = VolleySingleton.getInstance(getContext()).getRequestQueue();

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                      Log.d("Erro",error.toString());

                    }
                }){
                 @Override
                 public Map<String, String> getHeaders() {

                     Map<String, String> params = new HashMap<String, String>();
                     //params.put("cache-control", "no-cache");
                    // params.put("Connection","keep-alive");
                    // params.put("Content-Type","application/json");
                     //params.put("transfer-encoding","chunked");
                    // params.put("X-Frame-Options","deny");
                     return params;
                }

        };

        queue.add(jsObjRequest);

    }




    private class GetProducts extends AsyncTask<Void, Integer, String> {
        private String resp;


        protected void onPreExecute() {
            super.onPreExecute();

        }


        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }


        protected String doInBackground(Void... params) {
            String search_ajustado=setSearch(search);
            String url = String.format(Config.term, search_ajustado);

            try {
                HttpClient httpClient = new DefaultHttpClient();
                httpClient.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
                HttpGet httpPost = new HttpGet(url);
                HttpResponse response = httpClient.execute(httpPost);
                String responseBody = "";
                BufferedReader buffer = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String s = "";
                while ((s = buffer.readLine()) != null)
                    responseBody += s;

                resp=responseBody;


            } catch (Exception e) {e.printStackTrace();}


            return null;


        }


        protected void onPostExecute(String Resultado) {

            try {
                JSONObject json = new JSONObject(resp);
                JSONArray array = json.getJSONArray("products");
                JSONObject element = array.getJSONObject(0);
                String url_product = element.optString("link");
                url_product = url_product + "?json=true";
                new GetProductDetail(url_product).execute();

            }catch(Exception e){
                txtTitle.setText("Não foi possível localizar o produto procurado :(");
                txtTitle.setVisibility(View.VISIBLE);
                hsv.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                Log.d("Erro", "Erro");
            }

        }
    }




    private class GetProductDetail extends AsyncTask<Void, Integer, String> {
        private String resp;
        private String url;


        public GetProductDetail(String url) {
            this.url=url;
        }


        protected void onPreExecute() {
            super.onPreExecute();

        }


        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }


        protected String doInBackground(Void... params) {


            try {
                HttpClient httpClient = new DefaultHttpClient();
                httpClient.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
                HttpGet httpPost = new HttpGet(url);
                HttpResponse response = httpClient.execute(httpPost);
                String responseBody = "";
                BufferedReader buffer = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String s = "";
                while ((s = buffer.readLine()) != null)
                    responseBody += s;

                resp=responseBody;


            } catch (Exception e) {e.printStackTrace();}


            return null;


        }


        protected void onPostExecute(String Resultado) {
            try {
                JSONObject json = new JSONObject(resp);
                JSONObject product = json.getJSONObject("product");
                JSONObject erp = product.getJSONObject("erp_allocation");
                int section = erp.getInt("section");
                String depto = deptos.get(section);
                if (depto==null) depto="Não Localizado";

                String text = "A(O) <font color='#71BE44'>" + search + "</font> que você procura está em <font color='#71BE44'>" + depto + "</font>";
                txtTitle.setText(Html.fromHtml(text));
                txtTitle.setVisibility(View.VISIBLE);
                txtSubtitle.setVisibility(View.VISIBLE);
                hsv.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);


            }catch(Exception e){
                String text = "Houve um erro na pesquisa. Tente novamente mais tarde :(";
                txtTitle.setText(Html.fromHtml(text));
                txtTitle.setVisibility(View.VISIBLE);
                hsv.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                Log.d("Erro", e.toString());
            }

        }
    }


}
