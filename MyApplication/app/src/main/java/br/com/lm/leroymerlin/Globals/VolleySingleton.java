package br.com.lm.leroymerlin.Globals;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Claudio on 14/05/16.
 */
public class VolleySingleton {
    private static VolleySingleton instance=null;
    private RequestQueue requestQueue;


    private VolleySingleton(Context context){
        requestQueue = Volley.newRequestQueue(context);
    }

    public static VolleySingleton getInstance(Context context){
        if(instance==null){
            instance = new VolleySingleton(context);
        }

        return instance;
    }


    public RequestQueue getRequestQueue() {
        return requestQueue;
    }



}